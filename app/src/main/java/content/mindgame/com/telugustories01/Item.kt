package content.mindgame.com.telugustories01

 data class Item(
 val topic:String = "Topic Folder",
 val topic_title:String = "Topic Title",
 val menus:ArrayList<String>,
 val files:ArrayList<String>,
 val uris:ArrayList<String>

  ){

 }
