package content.mindgame.com.telugustories01

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.gson.Gson
import org.jetbrains.anko.*
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), AppInterfaces {

private var BANNER_LOADED = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)



        //Check Internet connection before loading the ad.
        loadBannerWithConnectivityCheck()
        //Initialize the Admob Object and save it so that anybody can access it.
        AdObject.INTERSTITIAL_ID = AdObject.INTERSTITIAL_TEST_ID
        AdObject.APPLICATION_ADS_ID = "ca-app-pub-3940256099942544~3347511713"
        AdObject.PACKAGE_NAME = packageName
        AdObject.admob = AdmobUtility(this,AdObject.ADS_MODE_PROD)

//        mLastFragment = supportFragmentManager.findFragmentByTag()
        val jsondata = applicationContext.assets.open(ItemDataset.APP_JSON_FILE_NAME).bufferedReader().readText()

        ItemDataset.items = streamingArray(jsondata)



        // If the app is in test mode load the test screen else load the start screen.
        if (AdObject.showAppOrNot()){
        if (AdObject.FRAGMENT_LOADED == true){
            AdObject.LAST_LOADED_SCREEN()
//            loadStartScreen()

        }else {
            loadStartScreen()
        }
        }
        else{
            loadTestModeScreen()
        }



    }

    override fun loadTestModeScreen(){
        if(!isFinishing) {
            val apply = supportFragmentManager.beginTransaction().apply {
                replace(R.id.frame_holder, TestModeFragment())
                commitAllowingStateLoss()
                addToBackStack(null)
            }

            this.loadBannerWithConnectivityCheck()
        }
    }
        override fun loadStartScreen(){
            if(!isFinishing) {
                val apply = supportFragmentManager.beginTransaction().apply {
                    replace(R.id.frame_holder, StartScreenFragment())
                    commitAllowingStateLoss()
                    addToBackStack(null)
                }
                loadBannerWithConnectivityCheck()
                AdObject.setLastLoaded { loadStartScreen() }
            }
    }

    override fun loadJokeTopics(){
        if(!isFinishing) {
            val apply = supportFragmentManager.beginTransaction().apply {
                replace(R.id.frame_holder, JokeHeaderFragment())
                commitAllowingStateLoss()
                addToBackStack(null)
            }
            loadBannerWithConnectivityCheck()
            AdObject.setLastLoaded { loadJokeTopics() }
        }
    }
    //Load the Item detail fragment
    override fun loadJokeDetail() {
        if(!isFinishing) {
            val apply = supportFragmentManager.beginTransaction().apply {
                replace(R.id.frame_holder, JokeDetailFragment())
                commitAllowingStateLoss()
                addToBackStack(null)
            }
            loadBannerWithConnectivityCheck()
            AdObject.setLastLoaded { loadJokeDetail() }
        }
    }

    override fun loadJokeMenus() {
        if(!isFinishing) {
        val apply = supportFragmentManager.beginTransaction().apply {
            replace(R.id.frame_holder, StoriesFragment())
            commitAllowingStateLoss()
            addToBackStack(null)
        }
        loadBannerWithConnectivityCheck()
        AdObject.setLastLoaded {loadJokeMenus()}
                    }
    }



    fun parseJSON(array: String):Boolean{

        doAsync {
            val jsonArray = streamingArray(array)

        }
        runOnUiThread {
            alert("JSON parsing done!") {
                yesButton { toast("Yat")
                noButton { toast("haha") }}
            }
        }
        return true
    }


    fun streamingArray(array: String): Collection<Item>  {


        val gson = Gson()

        // Deserialization
        val collectionType = object : TypeToken<Collection<Item>>(){}.type
        val result = gson.fromJson<Collection<Item>>(array, collectionType)


//
//        }
       return result
    }

    fun loadBannerWithConnectivityCheck(){
        if (AdObject.isConnected()) {
            findViewById<AdView>(R.id.adBanner).loadAd(AdRequest.Builder().build())
            BANNER_LOADED = true
        }
    }
}
