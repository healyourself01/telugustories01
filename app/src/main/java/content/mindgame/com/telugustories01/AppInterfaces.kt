package content.mindgame.com.telugustories01

interface AppInterfaces {
    fun loadJokeDetail()
    fun loadStartScreen()
    fun loadJokeTopics()
    fun loadJokeMenus()
    fun loadTestModeScreen()
}