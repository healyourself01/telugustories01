package content.mindgame.com.telugustories01

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.ActionBar
import android.support.v7.app.AppCompatActivity
import android.view.*
import android.webkit.WebView
import android.widget.Toast
import android.widget.Toolbar
import content.mindgame.com.telugustories01.R.id.my_toolbar
import kotlinx.android.synthetic.main.fragment_item.view.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [JokeDetailFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [JokeDetailFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class JokeDetailFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var itemTitle:MenuItem? =null
    private var wvWebView:WebView? = null
    lateinit var toolbar: android.support.v7.widget.Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_item, container, false)


/*----------------ACTIONS FOR TOOLBAR-------------------------*/
        //setup pthe action bar
        val act:AppCompatActivity = activity as AppCompatActivity
        v.my_toolbar.title = "Stories"
        act.setSupportActionBar(v.my_toolbar)

        setHasOptionsMenu(true)
        toolbar = v.my_toolbar

/*----------------ACTIONS FOR TOOLBAR-------------------------*/



        val position = ItemDataset.position
//        v.tvTitle2.text = "$position ." +ItemDataset.item_current.topic_title
//        v.wvBody.webViewClient = WebViewClient()
        v.tvTitle2.visibility = View.GONE
        wvWebView = v.wvBody
        wvWebView!!.settings.javaScriptEnabled = true
        wvWebView!!.loadUrl(ItemDataset.item_current.uris[position])
       //Share button
        v.imgbtnShare.setOnClickListener(View.OnClickListener {
            val sendIntent: Intent = Intent().apply {
                action = Intent.ACTION_SEND
                putExtra(Intent.EXTRA_TEXT, "Test data")
                type = "text/plain"
            }
            startActivity(sendIntent)
        })

        return v

    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        val mnu = inflater!!.inflate(R.menu.menu,menu)

        setHasOptionsMenu(true)
        super.onCreateOptionsMenu(menu, inflater)

    }

    override fun onPrepareOptionsMenu(menu: Menu?) {
        itemTitle = menu!!.findItem(R.id.tvItemTitle)
       toolbar.title = "${ItemDataset.position+1}."+ ItemDataset.item_current.menus[ItemDataset.position]
        super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
       when(item!!.itemId){
        R.id.next->{
        loadItem(1)


        }

           R.id.prev ->{
            loadItem(-1)
           }
       }
        return super.onOptionsItemSelected(item)
    }

    fun loadItem(index:Int){

       try
        {
            ItemDataset.position = ItemDataset.position + index
            toolbar.title = "${ItemDataset.position+1}."+ItemDataset.item_current.menus[ItemDataset.position]
                    AdObject.admob.loadNextScreen {

                        kotlin.run{wvWebView!!.loadUrl(ItemDataset.item_current.uris[ItemDataset.position])}
             }
        }
        catch (ex:ArrayIndexOutOfBoundsException){
            Toast.makeText(activity,"Last Item or First item reached!",Toast.LENGTH_SHORT).show()
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    }
